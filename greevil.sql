drop database if exists greevil;
create database greevil;
use greevil;


create table user (
    id int unsigned auto_increment primary key,
    `name` varchar(64),
    `balance` int unsigned default 0,
    `steamid64` varchar(32) not null UNIQUE,
    email varchar(128)  UNIQUE,
    password varchar(32),
    salt varchar(16),
    steamid32 varchar(16),
   	token	varchar(16),
    tokenChangePassWord varchar(32),
    secret_keyOTP varchar(16),
    lastip varchar(16),
    dateCreate int unsigned
);

-- CREATE TABLE `orders` (
-- `id` int unsigned auto_increment primary key,
-- order_id varchar(32) NOT NULL UNIQUE,
-- steamID varchar(32) NOT NULL, 
-- offer_id varchar(32),
-- rate int not null,
-- price float unsigned default 0 not null,
-- dateInsert int(12) UNSIGNED NOT NULL,
-- status enum('processing','offered','success','canceled') NOT NULL default 'processing',
-- refundBalance enum("","processing","refunded"),
-- offerErr varchar(512),
-- foreign key (`steamID`) references `user`(`steamID`)
-- );

CREATE TABLE `stock` (
`id` int unsigned auto_increment primary key,
botID64 varchar(16) not null,
itemID varchar(16) NOT NULL,
gameID varchar(10) not null,
particular JSON,
catalog_id int unsigned NOT NULL,
price float unsigned default 0,
dateInsert int(12) UNSIGNED NOT NULL,
availableShow enum('show','unshow') default 'unshow',
uuid varchar(32) not null,
status enum('instock','intrade','sold') NOT NULL default 'instock'
CHECK (JSON_VALID(particular))
);

create table trade_history (
    id int unsigned auto_increment primary key,
    order_id varchar(32) not null,
    offer_id varchar(32) not null, 
    steam_id varchar(32) not null,
    detaill_trade JSON,
    profit float,
    changeBalance int,
    status enum('processing','offered','success','canceled') NOT NULL default 'processing',
    trade_created int,
    last_update int,
    CHECK (JSON_VALID(detaill_trade))
);


-- detaill_trade : {botid : 1 ,Import : [{GameID : "570",ItemID : "52566",Nametag: "hihi",Sticker:"gg"}],Export:[{GameID : "570",ItemID : "52566",Nametag: "hihi",Sticker:"gg"}}


create table bot_manager (
    id int unsigned auto_increment primary key,
    steamid64   varchar(32) not null,
    steamid32   varchar(16) not null,
    token varchar(16) not null,
    username    varchar(32) not null,
    password    varchar(32) not null,
    api_key     varchar(32) not null,
    shared_key  varchar(32) not null,
    secret_key  varchar(32) not null,
    inventoryCSGO int default 0,
    inventoryDOTA2 int default 0,
    inventoryPUBG int default 0,
    ip_server varchar(16),
    status enum('offline','online') default 'offline',
    isAllow enum('enable','disable') default 'disable'
);

create table server_bot (
    id int unsigned auto_increment primary key,
    ip varchar(16) not null,
    status enum('stopped','running') default 'stopped'
);

-- particular (inspect varchar(64) null,
-- nameTag varchar(128) null,
-- floatValue float(12,12) null default 0,
-- patternIndex int unsigned null default 0,
-- metjimImg varchar(128) default "",`
-- ,
-- `stickerIDCatalog` varchar(128) not null,)



create table transaction_Detail (
    id int unsigned auto_increment primary key,
    steam_id varchar(32) not null,
    order_id varchar(32) not null,
    bankTrans enum('vcb','timo','card'),
    typeMobileCard varchar(7),
    mathamchieu text,
    `amount` int NOT NULL,
    paid int unsigned default 0,
    transactionType enum('topup','withdraw') NOT NULL,
    `date` int NOT NULL,
    status enum('waiting','success') default 'waiting'
);


create table wallet_transfer_log (
	id int unsigned auto_increment primary key,
	from_user varchar(32),
	to_user varchar(32),
    amount int,
    status enum('process','success') default 'process',
	dateInsert int 
);


-- create table balance (
--     balanceBank int not null default 0,
--     balance247 int not null default 0,
--     balancestock int not null default 0,
--     balance1pay int not null default 0
-- );


create table GDRutTien ( 
    id int unsigned auto_increment primary key,
    typePayment varchar(10),
    sothamchieu varchar(32) UNIQUE,
    amount int,
    ngayrut int
);

create table paymentUnknown (
    id int unsigned auto_increment primary key,
    typePayment varchar(10),
    sothamchieu varchar(32),
    amount int,
    memo text,
    dateInsert int
);



create table userAdmin (
    id int unsigned auto_increment primary key,
    username varchar(16) not null,
    password varchar(32) not null,
    salt varchar(32) not null,
    email varchar(32),
    otpSecrect varchar(16),
    activeOTP enum("enable","disable") default "disable",
    lastSignin int,
    banned enum('yes','no') default "yes",
    lastip varchar(24),
    role enum('admin','sale','support')
);

create table fundhistory (
    id int unsigned auto_increment primary key,
    adminid int not null,
    fundid varchar(32) not null,
    steamid varchar(16) not null,
    token varchar(16) not null,
    offerid varchar(32),
    listItem text not null,
    status enum("process","offered","cancel","success") default "process",
    dateInsert int unsigned,
    errorLog text
);
create table catalogCSGO (
    `id` int unsigned auto_increment primary key,
    `type` varchar(32) ,
    collection varchar(128),
    fullname varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    StattrakOrSouvenir enum ('StatTrak™','Souvenir','','★ StatTrak™') default '' ,
    `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    `skin` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    quality varchar(64),
    exterior enum('Factory New', 'Minimal Wear', 'Field-Tested','Well-Worn', 'Battle-Scarred','') default '',
    imgLink varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    priceImport float,
    priceExport float,
    ban enum('true','false') default 'false',
    update_at int unsigned,
    create_at int unsigned
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

create table catalogPUBG (
    `id` int unsigned auto_increment primary key,
    fullname varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    imgLink varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    priceImport float,
    priceExport float,
    ban enum('true','false') default 'false',
    update_at int unsigned,
    create_at int unsigned
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

create table catalogDOTA2
(
    `id` int unsigned auto_increment primary key ,
    `hero` varchar(32) ,
    `equip` varchar(64) ,
    fullname varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
     name varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
     rarity varchar(32) ,
    `sets` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci,
    treasure varchar(64),
    gem varchar(64), 
    imgLink varchar(128),
    priceImport float,
    priceExport float,
    ban enum('true','false') default 'false',
    update_at int unsigned,
    create_at int unsigned
)
CHARACTER SET utf8 COLLATE utf8_general_ci;



-- create user 'webapp'@'localhost' identified by 'Minhle(!&!)';
-- grant select on `kdcsgo`.`config` to 'webapp'@'localhost';  
-- grant select on `kdcsgo`.`user` to 'webapp'@'localhost';
-- grant insert on `kdcsgo`.`user` to 'webapp'@'localhost';
-- grant update on `kdcsgo`.`user` to 'webapp'@'localhost';
-- grant select on `kdcsgo`.`orders` to 'webapp'@'localhost';
-- grant update on `kdcsgo`.`orders` to 'webapp'@'localhost';
-- grant insert on `kdcsgo`.`orders` to 'webapp'@'localhost';
-- grant select on `kdcsgo`.`transaction_Detail` to 'webapp'@'localhost';
-- grant insert on `kdcsgo`.`transaction_Detail` to 'webapp'@'localhost';
-- grant select on `kdcsgo`.`stock` to 'webapp'@'localhost';
-- grant update on `kdcsgo`.`stock` to 'webapp'@'localhost';
-- grant select on `kdcsgo`.`catalogCSGO` to 'webapp'@'localhost'; 
-- grant select on `kdcsgo`.`balance` to 'webapp'@'localhost';
-- grant update on `kdcsgo`.`balance` to 'webapp'@'localhost';

-- FLUSH PRIVILEGES;

-- create user 'Admin'@'localhost' identified by 'stormshadows@!!!!(*';
-- grant all on `kdcsgo`.* to 'update'@'localhost';   
-- FLUSH PRIVILEGES;

-- grant all on `greevil`.* to 'update'@'localhost'; 
-- LOCK TABLES `userAdmin` WRITE;
-- INSERT INTO `userAdmin` VALUES (1,'Mon','b18a450d3f79bd31ab55e3d9c34d48db','WduqtCDMLxiDHIMG!FpXzp2LGIeh#p2s','Mon.Imitator@outlook.com','JGCKVUIBVJJYEPB6','enable',NULL,'no','113.161.149.77','admin');
-- UNLOCK TABLES;

-- LOCK TABLES `config` WRITE;
-- INSERT INTO `config` VALUES (3,'timo',2365,'{\"usertimo\":\"rampagewallet\",\"passtimo\": \"Minh(!&!(\"}','ggez','{\"Sotaikhoan\":\"9704321120414297\",\"TenNguoiNhan\": \"Le Quang Minh\"}','stopped'),(4,'vcb',2365,'{\"uservcb\":\"9713944a50\",\"passvcb\": \"Minh@!91719\",\"sotaikhoan\": \"0121000756695\",\"userdbc\": \"envilstore\",\"passdbc\":\"#Envil123\"}','ggez','{\"Sotaikhoan\":\"0121000756695\",\"TenNguoiNhan\": \"Le Quang Minh\"}','stopped'),(5,'rate',NULL,'15000',NULL,NULL,'stopped');

-- UNLOCK TABLES;
