var Web3 = require("web3");
var http = require("http");
//  var web3 = new Web3();
var Accounts = require("web3-eth-accounts");
var accounts = new Accounts();
http
  .createServer(function(request, response) {
    console.log(request.url);
    if (request.method === "POST") {
      if (request.url === "/private") {
        var requestBody = "0x";
        request.on("data", function(data) {
          if (data == "") {
            response.end("");
          }
          requestBody += data;
          var addressVi = accounts.privateKeyToAccount(requestBody);
          response.end(addressVi.address);
        });
      }
    }
    if (request.url === "/status") {
      response.end("");
    }
  })
  .listen(9000);
