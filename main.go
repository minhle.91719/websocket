package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/binance-exchange/go-binance"
	logg "github.com/go-kit/kit/log"
	"github.com/mrd0ll4r/tbotapi"
	"github.com/mrd0ll4r/tbotapi/examples/boilerplate"
)

func main() {
	var logger logg.Logger
	logger = logg.NewLogfmtLogger(logg.NewSyncWriter(os.Stderr))
	logger = logg.With(logger, "time", logg.DefaultTimestampUTC, "caller", logg.DefaultCaller)

	hmacSigner := &binance.HmacSigner{
		Key: []byte("eUz6iU0lb5xrPAGeweMTkLaFs6Pa0TzB1jKCD3n4HgNit23s1QssanRjcjWFKGPA"),
	}
	ctx, _ := context.WithCancel(context.Background())
	// use second return value for cancelling request when shutting down the app

	binanceService := binance.NewAPIService(
		"https://www.binance.com",
		"bKLCUDJsDJtorrRBH4VgAzdJPn95DxmFhYJ0rAXaXkOdk4lek5KBJSfMlyIFc2Qm",
		hmacSigner,
		logger,
		ctx,
	)
	b := binance.NewBinance(binanceService)
	// tickerBTC, _ := b.Ticker24(binance.TickerRequest{Symbol: "TRXBTC"})
	// fmt.Printf("%0.9f \n", tickerBTC.Volume*tickerBTC.WeightedAvgPrice)
	// fmt.Printf("%0.9f", tickerBTC.WeightedAvgPrice)
	// fmt.Println(tickerBTC.WeightedAvgPrice)
	// fmt.Println(ticker)

	// port, err := determineListenAddress()
	// if err != nil {
	// 	port = 80
	// }
	// fmt.Println(port)
	apiToken := "471561486:AAHHTu2MKUmyNReUuDxry6aZzH5nrvs9Y7k"
	botHost := "35.200.246.241"
	botPort := uint16(80)
	privkey := "bnb.key"
	pubkey := "bnb.pem"

	updateFunc := func(update tbotapi.Update, api *tbotapi.TelegramBotAPI) {
		switch update.Type() {
		case tbotapi.MessageUpdate:
			msg := update.Message
			typ := msg.Type()
			if typ != tbotapi.TextMessage {
				// Ignore non-text messages for now.
				fmt.Println("Ignoring non-text message")
				return
			}
			// Note: Bots cannot receive from channels, at least no text messages. So we don't have to distinguish anything here.

			var parseCommand = regexp.MustCompile(`/([a-z0-9]*)(@` + api.Username + `|)[ ]*([a-zA-Z]*)`)
			// Display the incoming message.
			// msg.Chat implements fmt.Stringer, so it'll display nicely.
			// We know it's a text message, so we can safely use the Message.Text pointer.
			fmt.Printf("<-%d, From:\t%s, Text: %s \n", msg.ID, msg.Chat, *msg.Text)
			parse := parseCommand.FindStringSubmatch(*msg.Text)
			if parse == nil {
				resp := "Command sai vcl"
				outMsg, err := api.NewOutgoingMessage(tbotapi.NewRecipientFromChat(msg.Chat), resp).Send()
				if err != nil {
					fmt.Printf("Error sending: %s\n", err)
					return
				}
				fmt.Printf("->%d, To:\t%s, Text: %s\n", outMsg.Message.ID, outMsg.Message.Chat, *outMsg.Message.Text)
			}
			switch parse[1] {
			case "t24":
				symbol := strings.ToUpper(parse[3]) + "BTC"
				fmt.Println(symbol)
				ticker, err := b.Ticker24(binance.TickerRequest{Symbol: symbol})
				if err != nil {
					log.Println(err)
					return
				}
				tickerBTC, _ := b.Ticker24(binance.TickerRequest{Symbol: "BTCUSDT"})
				respSym := strings.ToUpper(parse[3]) + "-" + "BTC"

				responseStr := "  " + respSym + "\n" +
					`Last Price: ` + fmt.Sprintf("%0.8f", ticker.LastPrice) + "(" + fmt.Sprintf("%0.2f USD", ticker.LastPrice*tickerBTC.LastPrice) + ")" + "\n" +
					"Change    : " + fmt.Sprintf("%0.8f", ticker.PriceChange) + " | " + fmt.Sprintf("%0.3f", ticker.PriceChangePercent) + " %\n" +
					"Volume 24h: " + AddDot(ticker.Volume*ticker.WeightedAvgPrice) + " BTC " + "(" + AddDot(ticker.Volume*ticker.WeightedAvgPrice*tickerBTC.LastPrice) + " USD)" + "\n" +
					"Low  Price: " + fmt.Sprintf("%0.8f", ticker.LowPrice) + "(" + fmt.Sprintf("%0.2f USD", ticker.LowPrice*tickerBTC.LastPrice) + ")" + "\n" +
					"High Price: " + fmt.Sprintf("%0.8f", ticker.HighPrice) + "(" + fmt.Sprintf("%0.2f USD", ticker.HighPrice*tickerBTC.LastPrice) + ")" + "\n"

				// Now simply echo that back.
				outMsg, err := api.NewOutgoingMessage(tbotapi.NewRecipientFromChat(msg.Chat), responseStr).Send()

				if err != nil {
					fmt.Printf("Error sending: %s\n", err)
					return
				}
				fmt.Printf("->%d, To:\t%s, Text: %s\n", outMsg.Message.ID, outMsg.Message.Chat, *outMsg.Message.Text)
			}

		case tbotapi.InlineQueryUpdate:
			fmt.Println("Ignoring received inline query: ", update.InlineQuery.Query)
		case tbotapi.ChosenInlineResultUpdate:
			fmt.Println("Ignoring chosen inline query result (ID): ", update.ChosenInlineResult.ID)
		default:
			fmt.Println("Ignoring unknown Update type.")
		}
	}

	// Run the bot, this will block.
	boilerplate.RunBotOnWebhook(apiToken, updateFunc, "Echo", "Echoes messages back", botHost, botPort, pubkey, privkey)
}
func determineListenAddress() (int, error) {
	port := os.Getenv("PORT")
	if port == "" {
		return 0, fmt.Errorf("$PORT not set")
	}
	portNum, _ := strconv.Atoi(port)
	return portNum, nil
}
func AddDot(number float64) string {
	newNum := ""

	tempI := int(number)
	for tempI >= (1000) {
		ins := fmt.Sprint(tempI % 1000)
		if len(ins) < 3 {
			ins = "0" + ins
		}
		newNum = "," + ins + newNum
		tempI /= 1000
	}
	newNum = fmt.Sprint(tempI) + newNum
	text := fmt.Sprintf("%0.2f", number-float64(int(number)))
	for i, vl := range text {
		if i == 0 {
			continue
		}
		newNum += string(vl)
	}
	return newNum
}
